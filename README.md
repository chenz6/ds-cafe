# This project is a test framework for DS-cafe
## Introduction
This project provides intuitive and easy ways to test. 

A User Class is provided in the `roles.py` file. The class has the following methods:
1. register
2. purchase
3. redeem
4. get_points

Get started from tests.py

## Quick Start

```python
from roles import User
from utils import fake_user
user = fake_user(1, same_group=False)[0]
user = User(user['username'], user['password'], user['email'], user['group_id'])
user.register()
user.purchase(purchase_value=100, store_id=1)
user.redeem(redeem_points=10, store_id=1)
user.get_points()
```