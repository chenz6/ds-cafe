from roles import User
from utils import fake_user


def test_run():
    # Fake some users
    fake_users = fake_user(5, same_group=False)
    print(fake_users)

    # pick a user
    user = fake_users[0]
    user = User(user['username'], user['password'], user['email'], user['group_id'])

    # Register the user
    print(user.register())
    assert user.user_id is not None
    print(user.user_id)

    # Purchase
    print(user.purchase(purchase_value=100, store_id=1))

    # Redeem
    print(user.redeem(redeem_points=10, store_id=1))

    # Get points
    print(user.get_points())


if __name__ == '__main__':
    test_run()