import argparse

from concurrency import ConcurrentSimulator

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Simulate concurrent point reads, updates and redemptions')

    parser.add_argument('--users', type=int, required=True, help='Number of users to perform the operation on.')
    parser.add_argument('--points', type=int, help='Total points to purchase/redeem.')
    parser.add_argument('--operation', type=str, choices=['purchase', 'redeem', 'read'], required=True, help='The operation to perform.')
    parser.add_argument('--use_existing_users', type=int, default=1, help='Create users before performing the operation.')

    args = parser.parse_args()
    
    if args.operation in ['purchase', 'redeem']:
        if args.points is None:
            print('Total points to purchase/redeem is required.')
            exit(1)

    simulator = ConcurrentSimulator()
    simulator.init(user_number=args.users, same_group=True, use_existing=args.use_existing_users)
    if args.operation == 'purchase':
        simulator.concurrent_point_update(total_points_to_purchase=args.points)
    elif args.operation == 'redeem':
        simulator.concurrent_point_redemption(total_points_to_redeem=args.points)
    elif args.operation == 'read':
        simulator.read_points_concurrently()
    else:
        print('Invalid operation.')