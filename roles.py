import json

from utils import api, fake_user
from dispatcher import Dispatcher
from const import registerUser, registerPurchase, redeemPoints, getGroupPoints, getGroupPurchaseHistory

class User:
    """
    1. register
    2. purchase
    3. redeem
    4. get_points
    """
    def __init__(self, username, password, email, group_id=None, user_id=None):
        self.username = username
        self.password = password
        self.email = email
        self.group_id = group_id
        self.user_id = user_id
        self.dispatcher = Dispatcher()

    def register(self):
        data = {
            'username': self.username,
            'password': self.password,
            'email': self.email,
            'groupId': self.group_id
        }
        try:
            status, response = self.dispatcher.wrap_update_request(registerUser, data)
            user_id = response.get('response').get('userSub')
            self.user_id = user_id
            return status, response
        except Exception as e:
            return None, str(e)

    def purchase(self, purchase_value, store_id):
        if not self.user_id:
            return None, 'User not registered'
        data = {
            'group_id': self.group_id,
            'user_id': self.user_id,
            'purchase_value': purchase_value,
            'store_id': store_id
        }
        try:
            status, response = self.dispatcher.wrap_update_request(registerPurchase, data, timeout=5, allow_local=True)
            return status, response
        except Exception as e:
            return None, str(e)

    def redeem(self, redeem_points, store_id):
        if not self.user_id:
            return None, 'User not registered'
        data = {
            'group_id': self.group_id,
            'user_id': self.username,
            'points': redeem_points,  # Assuming a fixed amount of points to redeem for simplicity
            'store_id': store_id  # Assuming a fixed store ID for redemption for simplicity
        }
        try:
            status, response = self.dispatcher.wrap_update_request(redeemPoints, data, timeout=5, allow_local=False)
            return status, response
        except Exception as e:
            return None, str(e)

    def get_points(self):
        if not self.user_id:
            return None, 'User not registered'
        data = {
            'group_id': self.group_id,
        }
        try:
            status, response = self.dispatcher.wrap_retrieve_request(getGroupPoints, data, timeout=5)
            return status, response
        except Exception as e:
            return None, str(e)
        
    def get_purchase_history(self):
        if not self.user_id:
            return None, 'User not registered'
        data = {
            'group_id': self.group_id,
            'user_id': self.user_id
        }
        try:
            status, response = self.dispatcher.wrap_retrieve_request(getGroupPurchaseHistory, data, timeout=5)
            return status, response
        except Exception as e:
            return None, str(e)


if __name__ == '__main__':
    # Example usage
    # user = fake_user(1, same_group=True)[0]
    # print(user)
    # user = User(**user)
    user = User('testuser5', 'Passw0rd!', 'new_user@example.com', 6, '30d16d7b-592d-486d-a958-38d6fc921508')
    # print(user.register())
    print(user.purchase(100, 789))
    # print(user.redeem(100, 789))
    # print(user.get_points())