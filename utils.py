import requests
import json
import random
import string


def api(url, data):
    """
    Sends a POST request to a specified URL with given data.

    Parameters:
    - url (str): The URL to which the POST request is sent.
    - data (dict): A dictionary containing the data to be sent in the POST request.

    Returns:
    - A tuple containing the response status code and the response body as a string.
    """
    # Convert the dictionary to a JSON string
    payload = json.dumps(data)

    # Headers to specify that the payload is in JSON format
    headers = {
        'Content-Type': 'application/json'
    }

    # Try to send a POST request
    try:
        response = requests.post(url, data=payload, headers=headers)
        # Return the response status code and body
        return response.status_code, response.text
    except Exception as e:
        # Return the error
        return None, str(e)


def fake_user(number, same_group=False):
    users = []

    random_group_id = lambda: ''.join(random.choices(string.digits, k=5))

    # Define common attributes for fake users
    common_attributes = {
        'email': 'user{}@example.com',
        'password': 'Passw0rd!123',
        'group_id': random_group_id() if same_group else None
    }

    for i in range(number):
        # Generate a random username
        username = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
        # Generate a unique email address
        email = common_attributes['email'].format(i)
        # Set group ID based on same_group parameter
        groupId = common_attributes['group_id'] if same_group else random_group_id()

        # Construct event dictionary
        event = {
            'username': username,
            'password': common_attributes['password'],
            'email': email,
            'group_id': int(groupId)
        }

        users.append(event)

    return users


def random_partition(number, parts):
    """
    Partitions a number into a given number of parts.
    :param number:
    :param parts:
    :return:
    """
    if parts == 1:
        return [number]
    partitions = sorted(random.sample(range(1, number), parts - 1))
    partition_sizes = [partitions[0]] + [partitions[i] - partitions[i - 1] for i in range(1, parts - 1)] + [number - partitions[-1]]
    return partition_sizes


if __name__ == '__main__':
    # Test the fake_user function
    fake_users = fake_user(5, same_group=False)
    print(fake_users)

