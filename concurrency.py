from concurrent.futures import ThreadPoolExecutor

from tqdm import tqdm

from roles import User
from utils import fake_user, random_partition


class ConcurrentSimulator:
    def __init__(self):
        self.users = []
        self.alice = User('alice', None, None, group_id=100, user_id='f09153ca-1c37-41f2-bde5-c4b20701ce6f')
        self.bob = User('bob', None, None, group_id=100, user_id='5416da69-ba03-4c58-aeb6-4ea7defe1d61')
        self.cynthia = User('cynthia', None, None, group_id=100, user_id='bc439a07-37cc-4ee7-9676-31646d440726')
        self.david = User('david', None, None, group_id=100, user_id='b2a45bd1-4010-48d3-8e82-743df53f2a9e')

    def init(self, user_number=3, same_group=True, use_existing=False):
        if use_existing:
            tmp_users = [self.alice, self.bob, self.cynthia, self.david]
            self.users = tmp_users[:user_number]
            return
        users = fake_user(user_number, same_group=same_group)
        for i, user in enumerate(users):
            user_obj = User(user['username'], user['password'], user['email'], user['group_id'])
            user_obj.register()
            users[i] = user_obj
        self.users = users

    def read_points(self):
        for user in self.users:
            status, response = user.get_points()
            resp_body = response.get('response', {'total_points': None})
            total_points = resp_body.get('total_points', None)
            print(f'{user.username}: {total_points}')

    def read_points_concurrently(self):
        futures = []
        with ThreadPoolExecutor() as executor:
            for user in self.users:
                futures.append(executor.submit(user.get_points))
        result_str = f'Points read by {len(self.users)} users concurrently: '
        for future in futures:
            status, response = future.result()
            resp_body = response.get('response', {'total_points': None})
            total_points = resp_body.get('total_points', None)
            result_str += f'{total_points} '
        print(result_str)

    def verify_point_update(self):
        # Read points before update
        print('Before points update:')
        self.read_points_concurrently()

        # Update points
        self.users[0].purchase(purchase_value=10, store_id=1)

        # Read points after update
        print('After points update:')
        self.read_points_concurrently()

    def concurrent_point_update(self, total_points_to_purchase=100, store_id=1):
        # Read points before update
        print('Before points update:')
        self.read_points_concurrently()

        print('Total points to purchase:', total_points_to_purchase)
        futures = []
        # distribute the purchase among the users
        purchase_values = random_partition(total_points_to_purchase, len(self.users))
        with ThreadPoolExecutor() as executor:
            for user, purchase_value in zip(self.users, purchase_values):
                futures.append(executor.submit(user.purchase, purchase_value=purchase_value, store_id=store_id))

        # Read points after update
        print('After points update:')
        self.read_points_concurrently()

    def concurrent_point_redemption(self, total_points_to_redeem=35, store_id=1):
        # Read points before redemption
        print('Before points redemption:')
        self.read_points_concurrently()

        futures = []
        # Redeem points concurrently
        redeem_points = random_partition(total_points_to_redeem, len(self.users))
        with ThreadPoolExecutor() as executor:
            for user, redeem in zip(self.users, redeem_points):
                futures.append(executor.submit(user.redeem, redeem_points=redeem, store_id=store_id))
        results = [future.result() for future in futures]
        for user, redeem, res in zip(self.users, redeem_points, results):
            print(f'{user.username}:  points to redeem: {redeem} \t {res}')

        # Read points after redemption
        print('After points redemption:')
        self.read_points_concurrently()
        return [res[1]['response']['status'] if 'response' in res[1] else 'failed' for res in results]


if __name__ == '__main__':
    # simulator.read_points()
    # simulator.concurrent_point_update(100)
    # simulator.verify_point_update()
    import time
    N = 10
    time_sum = 0
    PURCHASE = 100
    REDEEM = 110
    for _ in tqdm(range(N)):
        simulator = ConcurrentSimulator()
        simulator.init(user_number=3, same_group=True)
        simulator.concurrent_point_update(PURCHASE)
        s = time.time()
        res = simulator.concurrent_point_redemption(REDEEM)
        e = time.time()
        assert 'failed' in res, "Double Spending!"
        time_sum += e-s
        print("=====================================")
    print(f'Average time taken for concurrent requests: {time_sum/N:.2f} seconds')
    # simulator.concurrent_point_redemption()
